//
// Created by Pulsar on 2020/5/5.
//

#ifndef UESTC_CAREYE_RC_HTTPD_H
#define UESTC_CAREYE_RC_HTTPD_H

#include <cstdio>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cctype>
#include <cstring>
#include <cstring>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/wait.h>
#include <cstdlib>
//TODO:实现一个简单的WEB服务器用来显示和控制图像
#define ISspace(x) isspace((int)(x))
//函数说明：检查参数c是否为空格字符，
//也就是判断是否为空格(' ')、定位字符(' \t ')、CR(' \r ')、换行(' \n ')、
// 垂直定位字符(' \v ')或翻页(' \f ')的情况。
//返回值：若参数c 为空白字符，则返回非 0，否则返回 0。


#define SERVER_STRING "Server: jdbhttpd/0.1.0\r\n"//定义server名称
class rc_httpd {
private:
    void accept_request(int);//处理从套接字上监听到的一个 HTTP 请求
    void bad_request(int);//返回给客户端这是个错误请求，400响应码
    void cat(int, FILE *);//读取服务器上某个文件写到 socket 套接字
    void cannot_execute(int);//处理发生在执行 cgi 程序时出现的错误
    void error_die(const char *);//把错误信息写到 perror
    void execute_cgi(int, const char *, const char *, const char *);//运行cgi脚本，这个非常重要，涉及动态解析
    int get_line(int, char *, int);//读取一行HTTP报文
    void headers(int, const char *);//返回HTTP响应头
    void not_found(int);//返回找不到请求文件
    void serve_file(int, const char *);//调用 cat 把服务器文件内容返回给浏览器。
    int startup(u_short *);//开启http服务，包括绑定端口，监听，开启线程处理链接
    void unimplemented(int);//返回给浏览器表明收到的 HTTP 请求所用的 method 不被支持。
public:
    void start_server();//打开服务器
    void send_command();//发送命令到队列
    void init_server();//初始化WEB服务器
};


#endif //UESTC_CAREYE_RC_HTTPD_H
