//
// Created by pulsarv on 19-5-2.
//

#ifndef UESTC_CAREYE_RC_TCP_CLIENT_H
#define UESTC_CAREYE_RC_TCP_CLIENT_H

#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>


size_t read_complete(char * buf, const boost::system::error_code & err, size_t bytes);

void sync_echo(std::string msg);

#endif //UESTC_CAREYE_RC_TCP_CLIENT_H
