//
// Created by PulsarV on 18-5-14.
//

#ifndef ROBOCAR_RCTASKMANAGER_H
#define ROBOCAR_RCTASKMANAGER_H

#include <map>
#include <rc_move/rcmove.h>
#include <boost/thread/thread.hpp>
#include <rc_task/rcTaskManager_DataStruct.h>

namespace RC {
    namespace Task {

        /***
         * 设备启动
         * @param device_info
         */
        void run_robotclient_from_device(rc_DeviceInfo device_info);

        /***
         * 文件启动
         * @param device_info
         */
        void run_robotclient_from_file(rc_FDeviceInfo device_info);

        /**
         * 启动网络服务
         * @param device_info
         */
        void run_robotclient_network(rc_ServerInfo server_info);

        /**
         * 启动rtmp视频推流服务
         * @param server_info
         */
        void start_rtmp_server(rc_ServerInfo server_info);

        /**
         * 启动雷达服务
         * @param server_info
         */
        void start_radar_server(rc_DeviceInfo server_info);

        class TaskManager {
        public:
            explicit TaskManager(char *server_name);

            //从设备启动
            void init(int camera_index, const std::string& serial_port,
                      const std::string& mapping, std::string local_address, int local_port,
                      const std::string& remote_address, int remote_port);

            //从文件启动
            void init(const std::string& camera_file_path, const std::string& serial_port,
                      std::string mapping, const std::string& local_address, int local_port,
                      const std::string& remote_address, int remote_port);

            int start();

        private:
            bool is_init = false;
            char *server_name;
            rc_ThreadPool RobotServer;

        };
    }
}
#endif //ROBOCAR_RCTASKMANAGER_H
