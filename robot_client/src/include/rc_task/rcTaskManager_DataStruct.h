//
// Created by PulsarV on 18-5-14.
//

#ifndef ROBOCAR_RCTASKMANAGER_DATASTRUCT_H
#define ROBOCAR_RCTASKMANAGER_DATASTRUCT_H

#define CAMERA_TYPE int

#include <map>
#include <boost/thread/thread.hpp>

namespace RC {
    namespace Task {
        typedef boost::thread rc_Thread;
        typedef std::map<std::string, void *> rc_ThreadPool;
        typedef std::pair<std::string, void *> rc_ThreadUnion;

        typedef struct _rc_RadarInfo{
            int type;//雷达类型
            char *radar_device;//雷达地址
        } rc_RadarInfo;

        //视觉设备信息
        typedef struct _rc_DeviceInfo{
            int type=RC_CAMERA_DEVICE;
            char *serial_port;//制动板串口地址
            int camera_index;//摄像头编号
            char *mapping;//地图文件地址
        } rc_DeviceInfo;

        //文件信息
        typedef struct _rc_FDeviceInfo {
            int type = RC_VIDEO_FILE;
            char *serial_port;//制动板串口地址
            char *camera_file_path;//文件路径
            char *mapping;//地图文件地址
        } rc_FDeviceInfo;

        //服务器信息
        typedef struct _rc_ServerInfo {
            char *local_address;//本地绑定IP
            int local_port;//本地端口号
            char *rtmp_address;//视频推流地址
            int rtmp_port;//视频推流端口
            char *remote_address;//远程地址
            int remote_port;//远程端口号
            int port;//
        } rc_ServerInfo;

        //图像数据包
        typedef struct _rc_ImagePackage {
            int ID;
            char imagedata[1024 * 768 * 3];
        } rc_ImagePackage;

        //串口数据包
        typedef struct _rc_SerialPackage {
            char head;
            char sum_h;
            char sum_l;
        } rc_SerialPackage;
    }
}
#endif //ROBOCAR_RCTASKMANAGER_DATASTRUCT_H
