//
// Created by PulsarV on 18-5-10.
//

#ifndef ROBOCAR_RCSERIAL_H
#define ROBOCAR_RCSERIAL_H

#include <rc_log/rclog.h>
#include <fcntl.h>
#include <iostream>
#include <rc_globalVarable/rc_global_serial.h>
#include <rc_serial/imu_device/JY901.h>
#include <map>

#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <termios.h>
#include <zconf.h>
#include <linux/serial.h>

namespace RC {
    int set_serial(int fd, int nSpeed, int nBits, char nEvent, int nStop);

    typedef std::map<std::string, std::string> SERIAL_FLAGS;
    typedef std::pair<std::string, std::string> SERIAL_FLAG;

    class Serial {
    public:

        int open_serial(const std::string& serial_device_path);

        int send(const std::string& str);

        int send(long comm);

        int recive(char *buffer, int size);

        bool is_opend();

        int release();

        static int data_encode(int data);

        static int data_encode(char data);

        static int data_encode(std::string data);

    private:
        int device_point;
        char *device;
    };
}
#endif //ROBOCAR_RCSERIAL_H
