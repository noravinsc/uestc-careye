//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_RM_SERIAL_MSG_H
#define ROBOCAR_RM_SERIAL_MSG_H

#include <rc_task/rcTaskManager_DataStruct.h>
#include <rc_message/rc_base_msg.hpp>
#include <map>
namespace RC{
    namespace Message{
    class SerialMessage:public BaseMessage<std::map<int,Task::rc_SerialPackage>>{
        private:
            bool is_init= false;
            static boost::mutex::scoped_lock read_write_lock;
            static std::map<int,Task::rc_SerialPackage> serial_message;
    public:
            void init(std::string device,int freq);
            int send(int head,int size,char* message);
            int run();
            int recive();
        };
    }
}


#endif //ROBOCAR_RM_SERIAL_MSG_H
