//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_EC_IMAGE_MSG_H
#define ROBOCAR_EC_IMAGE_MSG_H

#include <rc_task/rc_message/rc_base_msg.hpp>
#include <rc_task/rcTaskManager_DataStruct.h>
#include <map>
namespace RC{
    namespace Message{

        class ImageMessage:public BaseMessage<std::map<int,ImagePackage>>{

        };
    }
}


#endif //ROBOCAR_EC_IMAGE_MSG_H
