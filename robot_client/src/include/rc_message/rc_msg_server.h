//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_RM_MSG_SERVER_H
#define ROBOCAR_RM_MSG_SERVER_H

#include <rc_message/rc_serial_msg.h>
#include <rc_message/rc_image_msg.h>
namespace RC{
    namespace Message{
        class MessageServer {
            static ImageMessage imageMessage;//图像消息
            static SerialMessage serialMessage;//串口消息
            static NetworkMessage networkMessage;//串口消息
        };
    }
}


#endif //ROBOCAR_RM_MSG_SERVER_H
