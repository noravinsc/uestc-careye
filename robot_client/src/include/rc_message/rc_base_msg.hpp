//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_RC_BASE_MSG_H
#define ROBOCAR_RC_BASE_MSG_H

#include <queue>

namespace RC {
    namespace Message {

        template<typename T>
        class BaseMessage {
        private:
            static std::queue<T> self_message_queue;
        public:
            T pop_message() {
                return self_message_queue.pop();
            }

            T front_message() {
                return self_message_queue.front();
            }

            T back_message() {
                return self_message_queue.back();
            }

            int size_message() {
                return self_message_queue.size();
            }

            void release_message() {
                while(self_message_queue.size()>0)
                    self_message_queue.pop();
            }
        };
    }
}
#endif //ROBOCAR_RC_BASE_MSG_H
