//
// Created by PulsarV on 18-5-9.
//

#ifndef ROBOCAR_RCMOVE_H
#define ROBOCAR_RCMOVE_H

#include <rc_cv/rcCV.h>
#include <rc_log/rclog.h>
#include <rc_serial/rcserial.h>
#include <rc_globalVarable/rc_global_wheel.h>
#include <rc_move/rcmove_data_struct.h>

namespace RC {
    class RobotCarMove {
    public:
        virtual int init(int camera_id, char *device, char *mapping);

        virtual int init(char *video, char *device, char *mapping);

        virtual int start();

        virtual void command(char com);

        virtual void wheel_1_forward(double trangle);

        virtual void wheel_1_backward(double trangle);

        virtual void wheel_2_forward(double trangle);

        virtual void wheel_2_backward(double trangle);

        virtual void wheel_3_forward(double trangle);

        virtual void wheel_3_backward(double trangle);

        virtual void wheel_go_forward();

        virtual void wheel_go_backward();

        virtual void wheel_stop();

        virtual void wheel_CW();//顺时针
        virtual void wheel_AC();//逆时针

        //编码控制数据
        virtual char *decode(
                bool weel_1_direction, unsigned short wheel_1_speed,
                bool weel_2_direction, unsigned short wheel_2_speed,
                bool weel_3_direction, unsigned short wheel_3_speed
        );

    private:
        int camera_id = -1;
        RC::Serial *serial_device{};
        char *video = NULL;
        int type = 0;
        char *mapping = NULL;
        bool AutoMove = false;
        bool AutoFollow = false;

        int init_serial_device(const std::string& device);
    };
}


#endif //ROBOCAR_RCMOVE_H
