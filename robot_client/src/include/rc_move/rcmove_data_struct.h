//
// Created by PulsarV on 18-5-9.
//

#ifndef ROBOCAR_RCMOVE_DATA_STRUCT_H
#define ROBOCAR_RCMOVE_DATA_STRUCT_H
//车轮控制数据
typedef struct {
    unsigned short wheel_1_speed;
    unsigned short wheel_2_speed;
    unsigned short wheel_3_speed;
    unsigned short sum=0;//校验和
    bool weel_2_direction;
    bool weel_1_direction;
    bool weel_3_direction;
}WHEEL_DATA;
#endif //ROBOCAR_RCMOVE_DATA_STRUCT_H
