//
// Created by pulsarv on 19-5-2.
//

#include <rc_network/rc_asny_tcp_client.h>
#include <iostream>

typedef boost::asio::ip::tcp::acceptor acceptor_type;
typedef boost::asio::ip::tcp::endpoint endpoint_type;
typedef boost::asio::ip::tcp::socket socket_type;
typedef boost::asio::ip::address address_type;
typedef boost::shared_ptr<socket_type> sock_ptr;
typedef std::vector<char> buffer_type;

namespace RC {
    namespace Network {


        rc_asny_tcp_client::rc_asny_tcp_client(boost::asio::ip::tcp::endpoint point)
                : m_io(), m_buf(100, 0), m_ep(point), socket(m_io) {
            std::cout << "\033[1;32;34m[INFO]    \033[0m连接服务器" <<point.address()<<":"<<point.port()<< std::endl;
            connect();
        }

        rc_asny_tcp_client::~rc_asny_tcp_client() {
            std::cout << "\033[1;32;34m[INFO]    \033[0m客户端退出" << std::endl;
        }

        bool rc_asny_tcp_client::run() {
            m_io.run();
            return true;
        }

        bool rc_asny_tcp_client::connect() {
            sock_ptr sock(new socket_type(m_io));
            sock->async_connect(m_ep,
                                boost::bind(&rc_asny_tcp_client::conn_handler, this, boost::asio::placeholders::error,
                                            sock));

            return true;
        }

        bool rc_asny_tcp_client::read_handler(const boost::system::error_code &ec,
                                              const boost::shared_ptr<boost::asio::ip::tcp::socket> sock) {
            if (ec) {
                std::cout << "异步读取错误！请检查配置" << std::endl;
                return false;
            }
            std::cout << &m_buf[0] << std::endl;
            return true;
        }

        bool rc_asny_tcp_client::conn_handler(const boost::system::error_code &ec,
                                              const boost::shared_ptr<boost::asio::ip::tcp::socket> &sock) {
            if (ec) {

                std::cout << "\033[1;32;31m[ERROR]   \033[0m异步连接错误！请检查配置" << std::endl;
                return false;
            }
            std::cout << "\033[1;32;34m[INFO]    \033[0m服务端信息：" << sock->remote_endpoint().address() << ":"
                      << sock->remote_endpoint().port() << std::endl;

            sock->async_read_some(boost::asio::buffer(m_buf),
                                  boost::bind(&rc_asny_tcp_client::read_handler, this,
                                              boost::asio::placeholders::error,
                                              sock)
            );

            return true;
        }

        bool rc_asny_tcp_client::reconnect() {
            sock_ptr sock(new socket_type(m_io));
            sock->close();
            socket.async_connect(m_ep, [this](const boost::system::error_code ec) {
                if (!ec) {
                    std::cout << "reconnected!" << std::endl;
                } else {
                    std::cout << "reconnect failed!" << std::endl;
                }
            });
        }

        bool rc_asny_tcp_client::readheart() {

            return false;
        }

        bool rc_asny_tcp_client::sendheart() {
            return false;
        }
    }
}