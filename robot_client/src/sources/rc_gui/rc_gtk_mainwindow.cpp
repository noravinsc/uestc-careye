//
// Created by PulsarV on 18-5-14.
//

#include <rc_globalVarable/rc_global.h>
#include <rc_gui/rc_gtk_mainwindow.h>
#include <rc_log/rclog.h>

RC::rc_gtk_mainwindow::rc_gtk_mainwindow() {
    GtkWidget *window;
    int i, screenNum;
    xcb_connection_t *connection = xcb_connect(NULL, &screenNum);
    const xcb_setup_t *setup = xcb_get_setup(connection);
    xcb_screen_iterator_t iter = xcb_setup_roots_iterator(setup);
    for (i = 0; i < screenNum; ++i) {
        xcb_screen_next(&iter);
    }
    xcb_screen_t *screen = iter.data;


//    LOG::logInfo("Screen Wdith " + to_string(screen->width_in_pixels));
//    LOG::logInfo("Screen Height " + to_string(screen->height_in_pixels));
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    //window_model = gtk_window_new(GTK_WINDOW_POPUP);
    gtk_window_set_title(GTK_WINDOW(window), "CarEyeSystem");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), screen->width_in_pixels, screen->height_in_pixels);
    gtk_container_set_border_width(GTK_CONTAINER(window), 5);
    g_signal_connect (G_OBJECT(window), "delete_event", G_CALLBACK("delete_event"), NULL);
    g_signal_connect (G_OBJECT(window), "destroy", G_CALLBACK("destroy"), NULL);

    GtkWidget *table = gtk_table_new(20, 4, TRUE);
    gtk_container_add(GTK_CONTAINER(window), table);

    GdkPixbuf *src = gdk_pixbuf_new_from_file(LOADING_IMAGE, NULL);
    GdkPixbuf *dst = gdk_pixbuf_scale_simple(src, 640, 480, GDK_INTERP_BILINEAR);
    GtkWidget *image = gtk_image_new_from_pixbuf(dst);
    GtkWidget *label_tempture = gtk_label_new("温度");
    GtkWidget *label_gps = gtk_label_new("GPS");
    GtkWidget *label_temp_now = gtk_label_new("天气");
    GtkWidget *speed = gtk_label_new("速度");
    GtkWidget *gama = gtk_label_new("gama波");
    GtkWidget *alpha = gtk_label_new("alpha波");
    GtkWidget *beta = gtk_label_new("beta波");
//    GtkWidget *weather_now = gtk_label_new(this->config["weather_model"].c_str());

    gtk_table_attach_defaults(GTK_TABLE(table), image, 0, 4, 0, 10);
    gtk_table_attach_defaults(GTK_TABLE(table), label_gps, 0, 2, 10, 11);
    gtk_table_attach_defaults(GTK_TABLE(table), label_temp_now, 2, 4, 10, 11);
    gtk_table_attach_defaults(GTK_TABLE(table), label_tempture, 0, 2, 11, 12);
    gtk_table_attach_defaults(GTK_TABLE(table), speed, 2, 4, 11, 12);

    gtk_table_attach_defaults(GTK_TABLE(table), alpha, 0, 1, 12, 13);
    gtk_table_attach_defaults(GTK_TABLE(table), beta, 1, 2, 12, 13);
    gtk_table_attach_defaults(GTK_TABLE(table), gama, 3, 4, 12, 13);

    gtk_table_attach_defaults(GTK_TABLE(table), weather_now, 0, 4, 13, 14);

    g_object_unref(src);  //free buff
    g_object_unref(dst);
    gtk_widget_show_all(window);
    this->singal.imageController = image;
    this->singal.gpsController = label_gps;
    this->singal.temp_nowController = label_temp_now;
    this->singal.temputerController = label_tempture;
    this->singal.speed = speed;
    this->singal.alpha = alpha;
    this->singal.beta = beta;
    this->singal.gama = gama;
    this->singal.video = &this->video;
    this->singal.config = this->config;

    g_timeout_add(50, changeState, (gpointer) &this->singal);
    g_timeout_add(100, changeImage, (gpointer) &this->singal);
    g_timeout_add(100, getSerinal, (gpointer) &this->singal);
    gtk_main();

}
