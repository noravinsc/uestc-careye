//
// Created by PulsarV on 18-5-9.
//

#include <rc_move/rcmove.h>
#include <cstring>
#include <base/slog.hpp>

int RC::RobotCarMove::init(int camera_id, char *device, char *mapping) {
    if (mapping != NULL)
        this->mapping = mapping;
    this->camera_id = camera_id;
    this->type = RC_PLAY_BY_CAMERA;
    this->init_serial_device(device);
    return 1;
}

int RC::RobotCarMove::init(char *video, char *device, char *mapping) {
    if (mapping != NULL)
        this->mapping = mapping;
    this->video = video;
    this->type = RC_PLAY_BY_VIDEO;
    this->init_serial_device(device);
    return 1;
}

int RC::RobotCarMove::init_serial_device(const std::string& device) {
    this->serial_device = new Serial();
    this->serial_device->open_serial(device);
    return 1;
}

int RC::RobotCarMove::start() {
//    std::fstream mapped;
//    std::ifstream map;

    cv::VideoCapture cap;
    switch (this->type) {
        case RC_PLAY_BY_CAMERA:
            if (this->camera_id != -1) {
                slog::info<<"Open Camera "<<this->camera_id<<" From Device"<<slog::endl;
                try {
                    cap.open(this->camera_id);
                }catch (cv::Exception &e){
                    slog::info<<"Open Camera "<<this->camera_id<<" Error"<<slog::endl;
                    return -1;
                }


            } else{
                slog::info<<RC_MOVE_DEVICE_PORT_INITATION_ERROR<<slog::endl;
                return -1;
            }

            break;
        case RC_PLAY_BY_VIDEO:
            if (this->video != nullptr) {
                LOG::logInfo((char *) "Open Camera Device From Files");
                cap.open(this->camera_id);
            } else{
                slog::err<<RC_MOVE_DEVICE_PORT_INITATION_ERROR<<slog::endl;
                return -1;

            }
            break;
    }

//    if (this->mapping != NULL) {
//        map.open(this->mapping);
//    } else {
//        mapped.open("map.bin", std::ios::app);
//    }

    if (cap.isOpened()) {
        LOG::logSuccess((char *) "Open Camera Device Successed");
        RC::CV::BodyDetceter decter;
        int load_cascade = decter.init_body_cascade(RC_BODY_CASCADES_FILE_PATH);
        while (true) {
            cv::Mat frame, output;
            cap >> frame;
            cv::Mat re_frame;
            cv::resize(frame, re_frame, cv::Size(128, 128), 0, 0, cv::INTER_LINEAR);

            if (not frame.empty()) {
                //巡线
                if (this->AutoMove) {
                    //TODO:寻线
                    int ans[2] = {0, 0};
                    cv::Mat thresh_image;
                    cv::Mat gray_image;
                    RC::CV::detcetByRightAndLeft(re_frame, ans);
                    if (this->serial_device->is_opend()) {
                        if (ans[0] > (128 / 2) + 10) {
                            this->wheel_AC();
                        }
                        if (ans[0] < (128 / 2) - 10) {
                            this->wheel_CW();
                        }
                        this->wheel_go_backward();
                        char buffer[64] = {'\0'};
                        this->serial_device->recive(buffer, 64);
                        std::string data = buffer;
                    }
                }
                //自动跟随
                if (this->AutoFollow && (load_cascade == 1)) {
                    cv::Point center, img_center;
                    img_center.x = frame.cols / 2;
                    img_center.y = frame.rows / 2;
                    std::vector<cv::Rect> body = decter.detcetBody(frame);
                    if (not body.empty()) {
                        center.x = body[0].x + cvRound(body[0].width / 2.0);
                        center.y = body[0].y + cvRound(body[0].height / 2.0);
                        cv::circle(frame, center, 3, cv::Scalar(0, 0, 255), -1);
                    }
                    cv::circle(frame, img_center, 10, cv::Scalar(255, 0, 0), -1);
                    if (this->serial_device->is_opend()) {
                        //TODO：转向修正
                    }
                }
            }

            cv::imshow("Origin Output", frame);
            char key = cv::waitKey(20);
            switch (key) {
                case 'c':
                    LOG::logInfo((char *) "Model Change to AutoMove");
                    this->AutoMove = this->AutoMove == true ? false : true;
                    this->AutoFollow = false;
                    break;
                case 'f':
                    LOG::logInfo((char *) "Model Change to AutoFllow");
                    this->AutoFollow = this->AutoFollow == true ? false : true;
                    this->AutoMove = false;
                    break;
                default:
                    break;
            }
//                if (map.is_open()) {
//                    if (!map.eof()) {
//                        char com = map.get();
//                        this->command(com);
//                        continue;
//                    }
//                }
            if (key == 'x')break;

//                if (mapping == NULL)
//                    if ((int) key > 65 and (int) key < 122)
//                        mapped << &key;
            if (this->serial_device->is_opend()) {
                this->command(key);
            }
//            }
        }
    } else
        return LOG::logError(RC_OPEN_CAMERA_ERROR);
    cv::destroyAllWindows();
//    mapped.close();
    return 1;
}

void RC::RobotCarMove::command(char com) {
    switch (com) {
        case 'w':
            this->wheel_go_backward();
            this->wheel_go_backward();
            break;
        case 'a':
            this->wheel_2_forward(20);
            this->wheel_2_forward(20);
            break;
        case 's':
            this->wheel_go_forward();
            this->wheel_go_forward();
            break;
        case 'd':
            this->wheel_1_backward(20);
            this->wheel_1_backward(20);
            break;
        case 'q':
            this->wheel_AC();
            this->wheel_AC();

            break;
        case 'e':
            this->wheel_CW();
            this->wheel_CW();
            break;
        case 'r':
            this->wheel_stop();
            this->wheel_stop();
            break;
    }
}

char *RC::RobotCarMove::decode(
        bool weel_1_direction, unsigned short wheel_1_speed,
        bool weel_2_direction, unsigned short wheel_2_speed,
        bool weel_3_direction, unsigned short wheel_3_speed) {
    WHEEL_DATA wheelData;
    wheelData.weel_1_direction = weel_1_direction;
    wheelData.weel_2_direction = weel_2_direction;
    wheelData.weel_3_direction = weel_3_direction;
    wheelData.wheel_1_speed = wheel_1_speed;
    wheelData.wheel_2_speed = wheel_2_speed;
    wheelData.wheel_3_speed = wheel_3_speed;
    wheelData.sum += weel_1_direction;
    wheelData.sum += weel_2_direction;
    wheelData.sum += weel_3_direction;
    wheelData.sum += wheel_1_speed;
    wheelData.sum += wheel_2_speed;
    wheelData.sum += wheel_3_speed;
    char *buffer = new char[sizeof(WHEEL_DATA)];
    memcpy(buffer, &wheelData, sizeof(WHEEL_DATA));
    return buffer;
}

