//
// Created by PulsarV on 18-5-14.
//
#include <rc_task/rcTaskManager.h>
#include <rc_log/rclog.h>
#include <zconf.h>
#include <rc_network/rc_asny_tcp_client.h>
#include <base/slog.hpp>

namespace RC {
    namespace Task {

        TaskManager::TaskManager(char *server_name) {
            this->server_name = server_name;
        }

        void start_rtmp_server(rc_ServerInfo server_info) {

        }

//网络通信模块
        void run_robotclient_network(rc_ServerInfo server_info) {
            slog::info << "Waiting for network...." << slog::endl;
            auto const address = boost::asio::ip::address_v4::from_string(server_info.remote_address);
            boost::asio::ip::tcp::endpoint point(address, server_info.remote_port);
            Network::rc_asny_tcp_client tcpClient(point);
            tcpClient.run();
        }

//从设备启动
        void run_robotclient_from_device(rc_DeviceInfo device_info) {
            slog::info << "Waiting for loading...." << slog::endl;
            RobotCarMove robot;
            robot.init(device_info.camera_index, device_info.serial_port, device_info.mapping);
            robot.start();
        }

//-------------------------------------------------------------
//从文件启动
        void run_robotclient_from_file(rc_FDeviceInfo device_info) {
            slog::info << "Waiting for loading...." << slog::endl;
            sleep(1);
            RobotCarMove robot;
            robot.init(device_info.camera_file_path, device_info.serial_port, device_info.mapping);
            robot.start();
        }
//-------------------------------------------------------------

//设备启动的初始化函数
        void TaskManager::init(int camera_index, const std::string& serial_port,
                               const std::string& mapping, std::string local_address,
                               int local_port,
                               const std::string& remote_address, int remote_port) {
            //视觉设备信息
            slog::info<<"camera_index:"<<camera_index<<slog::endl;
            slog::info<<"serial_port:"<<serial_port<<slog::endl;
            slog::info<<"mapping:"<<mapping<<slog::endl;

            slog::info<<"local_address:"<<local_address<<slog::endl;
            slog::info<<"local_port:"<<local_port<<slog::endl;

            slog::info<<"remote_address:"<<camera_index<<slog::endl;
            slog::info<<"remote_address:"<<remote_port<<slog::endl;

            rc_DeviceInfo device_info;
            device_info.serial_port = (char *) serial_port.c_str();
            device_info.camera_index = camera_index;
            device_info.mapping = (char *) mapping.c_str();

            //服务器信息
            rc_ServerInfo server_info;
            server_info.local_address = (char *) local_address.c_str();
            server_info.remote_address = (char *) remote_address.c_str();
            server_info.local_port = local_port;
            server_info.remote_port = remote_port;

            //启动网络模块
            slog::info << "Start Network Thread" << slog::endl;
//            rc_Thread robot_network_control_model(boost::bind(run_robotclient_network, server_info));

            //启动视觉模块
            slog::info << "Start Computer Version Thread" << slog::endl;
            rc_Thread robot_cv_control_model(boost::bind(run_robotclient_from_device, device_info));
            this->RobotServer.insert(rc_ThreadUnion("robot_cv_control_model", &robot_cv_control_model));


            //启动雷达模块
            slog::info << "Start Radar Thread" << slog::endl;

            //启动制动模块
            slog::info << "Start Robot Moving Thread" << slog::endl;

//    this->RobotServer.insert(rc_ThreadUnion("robot",t_robot));
//    robot_cv_control_model.join();
//    robot_network_control_model.join();
            this->is_init = true;
        }

//文件启动的初始化函数
        void TaskManager::init(const std::string& camera_file_path,
                               const std::string& serial_port, const std::string mapping,
                               const std::string& local_address, int local_port,
                               const std::string& remote_address, int remote_port) {
            rc_FDeviceInfo device_info;
            device_info.serial_port = (char *) serial_port.c_str();
            device_info.camera_file_path = (char *) camera_file_path.c_str();
            device_info.mapping = (char *) mapping.c_str();

            rc_ServerInfo server_info;
            server_info.local_address = (char *) local_address.c_str();
            server_info.remote_address = (char *) remote_address.c_str();
            server_info.local_port = local_port;
            server_info.remote_port = remote_port;
            slog::info << "Start Control Thread" << slog::endl;
//            boost::thread robot_cv_control_model(boost::bind(run_robotclient_network, server_info));
//    boost::thread robot_cv_control_model(boost::bind(run_robotclient_from_file, device_info));
//    this->RobotServer.insert(rc_ThreadUnion("robot_cv_control_model",robot_cv_control_model));
//    robot_cv_control_model.join();
            this->is_init = true;
        }

        int TaskManager::start() {
            if (this->is_init) {
                if (!this->RobotServer.empty()) {
                    std::map<std::string, void *>::iterator it;
                    it = RobotServer.begin();
                    while (it != RobotServer.end()) {
                        slog::info << "Service: " << it->first << slog::endl;
                        auto _thread = (rc_Thread *) it->second;
//                _thread->join();
                        it++;
                    }

                } else return false;
            } else {
                LOG::logError((char *) "You should run init before start");
                return -1;
            }
        }
    }
}