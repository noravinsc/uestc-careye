message(STATUS "Load RC Network")
set(LIB_NAME rcnetwork)
set(RCNETWORK_SOURCES_DIR ${PROJECT_SOURCE_DIR}/src/sources/rc_network)
set(RCNETWORK_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/src/include/rc_network)

file(GLOB RCNETWORK_INCLUDE ${RCNETWORK_INCLUDE_DIR}/*.h)
file(GLOB RCNETWORK_SOURCES ${RCNETWORK_SOURCES_DIR}/*.cpp)

foreach (FILE ${RCNETWORK_INCLUDE})
    message(STATUS ${FILE})
endforeach ()

foreach (FILE ${RCNETWORK_SOURCES})
    message(STATUS ${FILE})
endforeach ()
set(RC_NETWORK_FILES
        ${RCNETWORK_SOURCES}
        ${RCNETWORK_INCLUDE}
        )

