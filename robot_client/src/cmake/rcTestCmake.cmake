set(TEST_SOURCES_DIR ${PROJECT_SOURCE_DIR}/src/sources/rc_test)
################################(rc_serial_test)#######################################
if (RC_TEST)
    set(SERIAL_TEST ${TEST_SOURCES_DIR}/rc_serial_test.cpp)
    add_executable(rc_serial_test ${SERIAL_TEST})
    target_link_libraries(rc_serial_test ${OpenCV_LIBS} rc pthread)
    #######################################################################

    ################################(rc_cv_test)#######################################
    set(CV_TEST ${TEST_SOURCES_DIR}/rc_cv_test.cpp)
    add_executable(rc_cv_test ${CV_TEST})
    target_link_libraries(rc_cv_test ${OpenCV_LIBS} rc)
    #######################################################################

    ################################(rc_radar_test)#######################################
    set(RADAR_TEST ${TEST_SOURCES_DIR}/rc_radar_test.cpp)
    add_executable(rc_radar_test ${RADAR_TEST})
    target_link_libraries(rc_radar_test ${OpenCV_LIBS} rc)

    #######################################################################

    ################################(rc_radar_map_test)#######################################
    set(RADAR_MAP_TEST ${TEST_SOURCES_DIR}/rc_radar_map_test.cpp)
    add_executable(rc_radar_map_test ${RADAR_MAP_TEST})
    target_link_libraries(rc_radar_map_test ${OpenCV_LIBS} rc)
    #######################################################################

    ################################(rc_imu_ju901_test)#######################################
    set(RC_IMU_TEST ${TEST_SOURCES_DIR}/rc_imu_ju901_test.cpp)
    add_executable(rc_imu_ju901_test ${RC_IMU_TEST})
    target_link_libraries(rc_imu_ju901_test ${OpenCV_LIBS} rc)
    #######################################################################

    ################################(rc_mpi_test)#######################################
    set(RC_MPI_TEST ${TEST_SOURCES_DIR}/rc_mpi_test.cpp)
    add_executable(rc_mpi_test ${RC_MPI_TEST})
    find_package(MPI REQUIRED)
    target_link_libraries(rc_mpi_test ${OpenCV_LIBS} ${MPI_CXX_LIBRARIES} mpi)
    #######################################################################

    ################################(rc_cv_mpi_test)#######################################
    set(RC_CV_MPI_TEST ${TEST_SOURCES_DIR}/rc_cv_mpi_test.cpp)
    add_executable(rc_cv_mpi_test ${RC_CV_MPI_TEST})
    target_link_libraries(rc_cv_mpi_test ${OpenCV_LIBS} ${MPI_CXX_LIBRARIES} mpi)
    ################################(rc_cv_mpi_test)#######################################
    set(RC_GUI_GL_ON_GTK_TEST ${TEST_SOURCES_DIR}/rc_gl_on_gtk_test.cpp)
    add_executable(rc_gl_on_gtk_test ${RC_GUI_GL_ON_GTK_TEST})
    find_package(PkgConfig REQUIRED)

    pkg_check_modules(GTK3 REQUIRED gtk+-2.0)
    include_directories(${GTK2_INCLUDE_DIRS})
    link_directories(${GTK2_LIBRARY_DIRS})

    pkg_check_modules(GTKMM2 gtkmm-2.4)
    link_directories(${GTKMM2_LIBRARY_DIRS})
    include_directories(${GTKMM3_INCLUDE_DIRS})

    pkg_check_modules(GLEW glew)
    link_directories(${GLEW_LIBRARY_DIRS})
    include_directories(${GLEW_INCLUDE_DIRS})

    include_directories(/usr/include/gtkgl-2.0)
    target_link_libraries(rc_gl_on_gtk_test ${OpenCV_LIBS} ${OPENGL_LIBRARIES}
            ${GLUT_LIBRARY} ${MPI_CXX_LIBRARIES} ${GTK2_LIBRARIES}
            ${GTKMM2_LIBRARIES} GLEW GLU gtkgl-2.0)
    #######################################################################
    ################################(rc_socket_tcp_test)#######################################
    set(RC_TCP_TEST ${TEST_SOURCES_DIR}/rc_socket_tcp_test.cpp)
    add_executable(rc_socket_tcp_test ${RC_TCP_TEST})
    target_link_libraries(rc_socket_tcp_test ${OpenCV_LIBS} ${MPI_CXX_LIBRARIES} ${Boost_LIBRARIES} rc)
    #######################################################################

    ################################(rc_decode_test)#######################################
    set(RC_WHEEL_DECODE_TEST ${TEST_SOURCES_DIR}/rc_wheel_decode_test.cpp)
    add_executable(rc_wheel_decode_test ${RC_WHEEL_DECODE_TEST})
    target_link_libraries(rc_wheel_decode_test ${OpenCV_LIBS} ${MPI_CXX_LIBRARIES} ${Boost_LIBRARIES} rc)
    #######################################################################

endif ()
