message(STATUS "Load Message Module")
set(RCMESSAGE_DIR ${PROJECT_SOURCE_DIR}/src)
file(GLOB RC_MESSAGE_FILES ${RCMESSAGE_DIR}/sources/rc_message/*.cpp)
set(RC_MESSAGE_FILES
        ${RC_MESSAGE_FILES}
        )
foreach(FILE ${RC_MESSAGE_FILES})
    message(STATUS "RC_MESSAGE: ${FILE}")
endforeach()
include_directories(${RCMESSAGE_DIR}/include)



