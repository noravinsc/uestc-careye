message(STATUS "Load Task Module")
set(LIB_NAME rctask)
set(RCTASK_DIR ${PROJECT_SOURCE_DIR}/src)
file(GLOB RC_TASK_FILES ${RCTASK_DIR}/sources/rc_task/*.cpp)
foreach(FILE ${RC_TASK_FILES})
    message(STATUS "RC_TASK:${FILE}")
endforeach()
include_directories(${RCTASK_DIR}/include)

