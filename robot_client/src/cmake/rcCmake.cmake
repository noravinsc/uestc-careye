set(LIB_NAME rc)
set(RC_FILES
        ${RC_SERAIL_FILES}
        ${RC_TASK_FILES}
        ${RC_RTMP_FILES}
        ${RC_MOVE_FILES}
        ${RC_MAPPING_FILES}
        ${RC_GUI_FILES}
        ${RC_CV_FILES}
        ${RC_JSON_FILES}
        ${RC_LOG_FILES}
        ${RC_NNET_FILES}
        ${RC_NETWORK_FILES}
        )
add_library(${LIB_NAME} SHARED ${RC_FILES})

    target_link_libraries(${LIB_NAME} ${OpenCV_LIBS}
            boost_system
            boost_thread
            pthread
            ${OPENGL_LIBRARIES}
            ${GLUT_LIBRARY}
            glib-2.0
            stdc++
            m
            ${MPI_CXX_LIBRARIES}
            )

