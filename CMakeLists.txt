cmake_minimum_required(VERSION 3.0)
set(CMAKE_CXX_STANDARD 11)
option(WITH_CONTROL_CENTER "Build Robot Control Center" ON)
set(RC_LIB ON)

####################################
## to use C++11
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
if (${CMAKE_CXX_COMPILER_ID} STREQUAL GNU)
    set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")
endif ()
set(CMAKE_EXE_LINKER_FLAGS  "-fPIC -no-pie")

####################################


add_subdirectory(3rdparty/gflags)
#add_subdirectory(3rdparty/grpc)
add_subdirectory(3rdparty/libserv)

add_definitions(-D DEBUG)
add_subdirectory(robot_client)

#if (${WITH_CONTROL_CENTER} STREQUAL "ON")
#    add_subdirectory(robot_center)
#endif ()

